
# Teams

* [Allgemein](https://teams.microsoft.com/_#/conversations/Allgemein?threadId=19:7821bb6583a8441c8224574b71b09b17@thread.skype&ctx=channel)

* [Schichten](https://teams.microsoft.com/_#/apps/42f6c1da-a241-483a-a3cc-4f5be9185951/sections/shifts)


# Jira

* [Overview](https://pwdeag.atlassian.net/browse/DEV)

* [Jira Workflow](https://pfwdeag.sharepoint.com/:w:/r/sites/dev-int/_layouts/15/doc2.aspx?sourcedoc=%7B395A40CE-32A3-4E28-ABF1-D5C24A25A6B1%7D&file=Jira%20Workflow.docx)


# Sharepoint

* [Overview](https://pfwdeag.sharepoint.com/sites/dev-int)

* [Streaming](https://pfwdeag.sharepoint.com/sites/dev-int/SitePages/Streaming.aspx)

* [Ops](https://pfwdeag.sharepoint.com/sites/dev-int/SitePages/Ops-Support.aspx?source=https%3A%2F%2Fpfwdeag.sharepoint.com%2Fsites%2Fdev-int%2FSitePages%2FForms%2FByAuthor.aspx)

* [Jira Workflow](https://pfwdeag.sharepoint.com/:w:/r/sites/dev-int/_layouts/15/doc2.aspx?sourcedoc=%7B395A40CE-32A3-4E28-ABF1-D5C24A25A6B1%7D&file=Jira%20Workflow.docx)


# Stack Overflow

* [Questions](https://stackoverflow.com/c/accendere/questions)

* [Trikots/Dresses ändern?](https://stackoverflow.com/c/accendere/questions/124)

* [Trikots für England, Irland und Südafrika?](https://stackoverflow.com/c/accendere/questions/20)


# Bitbucket

* [Overview](https://bitbucket.org/dashboard/overview)

* [Pull Requests](https://bitbucket.org/dashboard/pullrequests)

* [Repos](https://bitbucket.org/dashboard/repositories)


# Google

* [Mail](https://mail.google.com/mail/u/0/#inbox)

* [Calandar](https://calendar.google.com/calendar/u/0/r/week)

# Elasticsearch

* [Indices](http://192.168.253.56:5601/app/management/data/index_management/indices)

* [Alerts](http://192.168.253.56:5601/app/management/insightsAndAlerting/triggersActions/alerts)

* [Users](http://192.168.253.56:5601/app/management/security/users)

* [API Console](http://192.168.253.56:5601/app/dev_tools#/console)

* [Elastic Stack Doku](https://bitbucket.org/p3ls/es-mgr/src/master/docs/README.md)

### Logs

* [Kidron Production](http://192.168.253.56:5601/s/kidron/app/logs)

* [Test](http://192.168.253.56:5601/s/test/app/logs/stream)

# Unsorted

* [Grafana](https://grafana.pfwiaas.com)

* [Heftpflaster](http://heftpflaster.k8s.intern.pfwiaas.com/login)

* [API Clients](http://api-lan.pfwiaas.com/app_status/client/client.php)

* [Jenkins Dev](http://jenkins-dev.intern.pfwiaas.com/login)

* [Rancher Dev](https://kubernetes-dev01.intern.pfwiaas.com:8443/login)

* [ptr.pferdewetten.de](http://ptr.pferdewetten.de)


# Projects


## Dark Ronald

* [Rennen](https://drda.pfwiaas.com/races)

* [Events](https://drda.pfwiaas.com/races/events)

* [Bets](https://drda.pfwiaas.com/bets/overview)


* [dvl02-drd](https://dvl02-drd.pfwiaas.com/)

* [Repo](https://bitbucket.org/accendere/darkronald/src/master/readme.md)


## Pythia

[feed-consumer](https://bitbucket.org/accendere/ms-go-pythia-feed-consumer/src/master/README.md)

[bet-processor](https://bitbucket.org/accendere/ms-go-pythia-bet-processor/src/master/README.md)
    * [Dev Instance - http://pythia.k8s.intern.pfwiaas.com/bet](http://pythia.k8s.intern.pfwiaas.com/bet)
    * [Mantis Ticket, Anbindung an Kidron/Sec, Bullet Points](https://mantis.pfwiaas.com/view.php?id=648)
    * [Mantis Ticket offene Fragen](https://mantis.pfwiaas.com/view.php?id=1120)


## [ms-go-sportwelt]((https://bitbucket.org/accendere/ms-go-sportwelt)
    * [Dev instance - http://192.168.253.171:30803](http://192.168.253.171:30803)



# Server

* [ptr01 - 192.168.253.92](192.168.253.92)

* [web02 - 192.168.253.153](192.168.253.153)

* [elastic-stack01 - 192.168.253.56](192.168.253.56)




# Veraltet

* [Graylog](https://graylog.pfwiaas.com/system/nodes)

* [Mantis](https://mantis.pfwiaas.com/my_view_page.php)